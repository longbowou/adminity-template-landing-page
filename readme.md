# Diversity Landing Page
[Adminty](https://colorlib.com//polygon/adminty/files/extra-pages/landingpage/index.html) Bootstrap 4 Admin Landing page Template

## Screen Shots
![](screen_shots/home-1.png)

![](screen_shots/home-2.png)

![](screen_shots/home-3.png)

![](screen_shots/home-4.png)

![](screen_shots/home-5.png)

![](screen_shots/home-6.png)